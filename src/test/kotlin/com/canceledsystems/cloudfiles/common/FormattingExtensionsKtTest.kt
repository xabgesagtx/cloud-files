package com.canceledsystems.cloudfiles.common

import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test

internal class FormattingExtensionsKtTest {

    @Test
    fun `toSizeString zero`() {
        MatcherAssert.assertThat(0L.toSizeString(), equalTo("0 B"))
    }

    @Test
    fun `toSizeString one`() {
        MatcherAssert.assertThat(1L.toSizeString(), equalTo("1 B"))
    }

    @Test
    fun `toSizeString less than kb`() {
        MatcherAssert.assertThat(1023L.toSizeString(), equalTo("1023 B"))
    }

    @Test
    fun `toSizeString one kb`() {
        MatcherAssert.assertThat(1024L.toSizeString(), equalTo("1.00 KB"))
    }

    @Test
    fun `toSizeString less than mb`() {
        MatcherAssert.assertThat((1023L * 1024).toSizeString(), equalTo("1023.00 KB"))
    }

    @Test
    fun `toSizeString one mb`() {
        MatcherAssert.assertThat((1024L * 1024).toSizeString(), equalTo("1.00 MB"))
    }

    @Test
    fun `toSizeString less than gb`() {
        MatcherAssert.assertThat((1023L * 1024 * 1024).toSizeString(), equalTo("1023.00 MB"))
    }

    @Test
    fun `toSizeString one gb`() {
        MatcherAssert.assertThat((1024L * 1024 * 1024).toSizeString(), equalTo("1.00 GB"))
    }

    @Test
    fun `toSizeString less than tb`() {
        MatcherAssert.assertThat((1023L * 1024 * 1024 * 1024).toSizeString(), equalTo("1023.00 GB"))
    }

}