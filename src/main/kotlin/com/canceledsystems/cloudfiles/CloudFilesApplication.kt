package com.canceledsystems.cloudfiles

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity

@SpringBootApplication
@EnableMethodSecurity
class CloudFilesApplication

fun main(args: Array<String>) {
	runApplication<CloudFilesApplication>(*args)
}
