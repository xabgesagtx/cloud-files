package com.canceledsystems.cloudfiles.pme.web

import com.canceledsystems.cloudfiles.pme.service.PmeFileService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.server.ResponseStatusException

@Controller
@RequestMapping("pme")
class PmeController(private val fileService: PmeFileService) {

    @GetMapping("folder/{folderId}")
    fun list(@PathVariable folderId: String, model: Model): String {
        model.addAttribute("folder", fileService.listFolder(folderId) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find folder $folderId"))
        return "pme/list"
    }

    @GetMapping("folder")
    fun baseFolder(): String {
        return "redirect:/pme/folder/0"
    }
}