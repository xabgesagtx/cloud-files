package com.canceledsystems.cloudfiles.pme.external

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy::class)
data class PmeApiFolderResponse(val status: String,
                                val content: List<PmeApiFolderItem>,
                                val name: String,
                                val parentId: String,
                                val folderId: String,
                                val breadcrumbs: List<PmeApiBreadCrumb>)