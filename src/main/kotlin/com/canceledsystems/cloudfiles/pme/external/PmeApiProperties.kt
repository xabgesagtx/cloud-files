package com.canceledsystems.cloudfiles.pme.external

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("pme.api")
class PmeApiProperties {
    lateinit var key: String
    lateinit var baseUrl: String
}