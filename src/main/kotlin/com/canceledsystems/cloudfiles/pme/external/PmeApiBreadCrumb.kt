package com.canceledsystems.cloudfiles.pme.external

data class PmeApiBreadCrumb(val id: String, val name: String)
