package com.canceledsystems.cloudfiles.pme.external

import org.slf4j.LoggerFactory
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Component
import org.springframework.web.client.RestClientException
import org.springframework.web.util.UriComponentsBuilder

@Component
class PmeApiClient(restTemplateBuilder: RestTemplateBuilder,
                   private val properties: PmeApiProperties) {

    private val log = LoggerFactory.getLogger(javaClass)
    private val restTemplate = restTemplateBuilder.build()

    fun fileDetails(fileId: String): PmeApiFileSummary? {
        val uri = UriComponentsBuilder.fromUriString(properties.baseUrl)
                .path("item/details")
                .queryParam("apikey", properties.key)
                .queryParam("id", fileId)
                .build()
                .toUri()
        return try {
            restTemplate.getForObject(uri, PmeApiFileSummary::class.java)
        } catch (e: RestClientException) {
            log.info("Failed to get details for file $fileId", e)
            null
        }
    }

    fun listFolder(folderId: String): PmeApiFolderResponse? {
        val uriBuilder = UriComponentsBuilder.fromUriString(properties.baseUrl)
                .path("folder/list")
                .queryParam("apikey", properties.key)
                .queryParam("includebreadcrumbs", true)
        if (folderId != "0") {
            uriBuilder.queryParam("id", folderId)
        }
        val uri = uriBuilder.build().toUriString()
        return try {
            restTemplate.getForObject(uri, PmeApiFolderResponse::class.java)
        } catch (e: RestClientException) {
            log.info("Failed to list folder $folderId", e)
            null
        }
    }
}