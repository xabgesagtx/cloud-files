package com.canceledsystems.cloudfiles.pme.external

import com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(SnakeCaseStrategy::class)
data class PmeApiFileSummary(val id: String,
                             val name: String,
                             val mimeType: String,
                             val link: String,
                             val size: Long)