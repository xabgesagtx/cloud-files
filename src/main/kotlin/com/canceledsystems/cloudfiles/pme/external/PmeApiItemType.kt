package com.canceledsystems.cloudfiles.pme.external

enum class PmeApiItemType {
    file, folder
}