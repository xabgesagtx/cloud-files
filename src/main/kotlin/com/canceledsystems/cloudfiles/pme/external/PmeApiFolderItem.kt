package com.canceledsystems.cloudfiles.pme.external

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy::class)
data class PmeApiFolderItem(val id: String,
                            val name: String,
                            val type: PmeApiItemType,
                            val size: Long?,
                            val createdAt: Long?,
                            val mimeType: String?)