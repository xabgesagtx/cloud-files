package com.canceledsystems.cloudfiles.pme.service

import com.canceledsystems.cloudfiles.common.toSizeString
import java.time.Instant

data class PmeFile(val id: String,
                   val name: String,
                   val size: Long,
                   val createdAt: Instant,
                   val mimeType: String) : PmeFolderItem {
    val sizeString: String
        get() {
            return size.toSizeString()
        }
    override val type = "file"
}