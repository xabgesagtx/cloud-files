package com.canceledsystems.cloudfiles.pme.service

data class PmeFolder(val id: String,
                     val name: String) : PmeFolderItem {
    override val type = "folder"
}