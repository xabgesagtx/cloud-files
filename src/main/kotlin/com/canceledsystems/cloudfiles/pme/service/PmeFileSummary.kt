package com.canceledsystems.cloudfiles.pme.service

import java.net.URI

data class PmeFileSummary(val id: String,
                          val name: String,
                          val mimeType: String,
                          val link: URI,
                          val size: Long)