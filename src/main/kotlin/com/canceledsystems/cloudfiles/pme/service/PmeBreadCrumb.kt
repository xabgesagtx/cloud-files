package com.canceledsystems.cloudfiles.pme.service

data class PmeBreadCrumb(val id: String, val name: String)