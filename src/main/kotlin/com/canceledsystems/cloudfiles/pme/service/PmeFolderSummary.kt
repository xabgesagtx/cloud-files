package com.canceledsystems.cloudfiles.pme.service

data class PmeFolderSummary(val id: String,
                            val name: String,
                            val parentId: String,
                            val breadcrumbs: List<PmeBreadCrumb>,
                            val items: List<PmeFolderItem>)