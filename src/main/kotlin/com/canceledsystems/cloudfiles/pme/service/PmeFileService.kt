package com.canceledsystems.cloudfiles.pme.service

import com.canceledsystems.cloudfiles.pme.external.PmeApiBreadCrumb
import com.canceledsystems.cloudfiles.pme.external.PmeApiClient
import com.canceledsystems.cloudfiles.pme.external.PmeApiFolderItem
import com.canceledsystems.cloudfiles.pme.external.PmeApiItemType
import org.springframework.stereotype.Service
import java.net.URI
import java.time.Instant

@Service
class PmeFileService(private val apiClient: PmeApiClient) {

    fun fileDetails(fileId: String): PmeFileSummary? {
        return apiClient.fileDetails(fileId)?.let {
            PmeFileSummary(id = it.id,
                    name = it.name,
                    mimeType = it.mimeType,
                    link = URI.create(it.link),
                    size = it.size)
        }
    }

    fun listFolder(folderId: String): PmeFolderSummary? {
        return apiClient.listFolder(folderId)?.let {
            PmeFolderSummary(id = it.folderId,
                    parentId = it.parentId,
                    name = it.name,
                    items = it.content.map { item -> item.convert() },
                    breadcrumbs = it.breadcrumbs.map { bc -> bc.convert() }
            )
        }
    }

}

private fun PmeApiFolderItem.convert(): PmeFolderItem {
    return if (type == PmeApiItemType.file) {
        toFile()
    } else {
        toFolder()
    }
}

private fun PmeApiFolderItem.toFile(): PmeFile {
    return PmeFile(id = id,
            name = name,
            size = size ?: throw IllegalArgumentException("size should not be null"),
            createdAt = Instant.ofEpochSecond(createdAt
                    ?: throw IllegalArgumentException("createdAt should not be null")),
            mimeType = mimeType ?: throw IllegalArgumentException("mimeType should not be null")
    )
}

private fun PmeApiFolderItem.toFolder(): PmeFolder {
    return PmeFolder(id = id,
            name = name)
}

private fun PmeApiBreadCrumb.convert() = PmeBreadCrumb(id, name)
