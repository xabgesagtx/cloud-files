package com.canceledsystems.cloudfiles.common.web

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("home")
class HomeProperties {

    var greetingText = ""
}