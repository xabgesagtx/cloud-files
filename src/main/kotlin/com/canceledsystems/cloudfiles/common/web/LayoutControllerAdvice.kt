package com.canceledsystems.cloudfiles.common.web

import jakarta.servlet.http.HttpServletRequest
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ModelAttribute

@ControllerAdvice
class LayoutControllerAdvice {

    @ModelAttribute("requestPath")
    fun requestPath(request: HttpServletRequest): String? = request.requestURI

}