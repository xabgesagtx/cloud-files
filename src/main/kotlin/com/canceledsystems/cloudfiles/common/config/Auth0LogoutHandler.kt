package com.canceledsystems.cloudfiles.common.config

import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder
import java.io.IOException


@Component
class Auth0LogoutHandler(
    private val securityProperties: SecurityProperties,
    private val clientRegistrationRepository: ClientRegistrationRepository
) :
    SecurityContextLogoutHandler() {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun logout(
        httpServletRequest: HttpServletRequest?, httpServletResponse: HttpServletResponse,
        authentication: Authentication
    ) {
        super.logout(httpServletRequest, httpServletResponse, authentication)

        val issuer = getClientRegistration().providerDetails.configurationMetadata["issuer"] as String?
        val clientId = getClientRegistration().clientId
        val returnTo = securityProperties.logoutSuccessUrl
        val logoutUrl = UriComponentsBuilder
            .fromUriString(issuer + "v2/logout?client_id={clientId}&returnTo={returnTo}")
            .encode()
            .buildAndExpand(clientId, returnTo)
            .toUriString()
        try {
            httpServletResponse.sendRedirect(logoutUrl)
        } catch (ioe: IOException) {
            log.warn("Failed to redirect to logout url", ioe)
        }
    }

    private fun getClientRegistration(): ClientRegistration {
        return clientRegistrationRepository.findByRegistrationId("auth0")
    }
}