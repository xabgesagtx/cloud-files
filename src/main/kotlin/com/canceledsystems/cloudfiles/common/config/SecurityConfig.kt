package com.canceledsystems.cloudfiles.common.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.web.SecurityFilterChain


@Configuration
internal class SecurityConfig(private val logoutHandler: Auth0LogoutHandler) {

    @Bean
    fun configure(http: HttpSecurity): SecurityFilterChain {
        http.authorizeHttpRequests {
            it.requestMatchers("/cloud/**")
                .hasAuthority("SCOPE_cloudfiles")
                .requestMatchers("/pme/**", "/imports/**")
                .hasAuthority("SCOPE_cloudfiles_admin")
                .anyRequest()
                .permitAll()
        }
        http.logout {
            it.logoutUrl("/logout")
                .permitAll()
                .addLogoutHandler(logoutHandler)
        }
        http.oauth2Client(Customizer.withDefaults())
        http.oauth2Login(Customizer.withDefaults())
        return http.build()
    }
}