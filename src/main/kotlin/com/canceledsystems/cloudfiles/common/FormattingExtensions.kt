package com.canceledsystems.cloudfiles.common

import java.util.*
import kotlin.math.ln
import kotlin.math.pow

fun Long.toSizeString(): String {
    return if (this < 1024) {
        "$this B"
    } else {
        val unit = 1024
        val exp = (ln(toDouble()) / ln(unit.toDouble())).toInt()
        val pre = "KMGTPE"[exp - 1].toString()
        String.format(Locale.ENGLISH, "%.2f %sB", this / unit.toDouble().pow(exp.toDouble()), pre)
    }
}