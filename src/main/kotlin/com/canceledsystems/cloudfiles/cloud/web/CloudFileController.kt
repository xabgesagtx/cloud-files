package com.canceledsystems.cloudfiles.cloud.web

import com.canceledsystems.cloudfiles.cloud.service.CloudFileService
import com.canceledsystems.cloudfiles.cloud.service.CloudFileSummary
import com.canceledsystems.cloudfiles.common.toSizeString
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus.FOUND
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes

@Controller
@RequestMapping("cloud")
class CloudFileController(private val cloudFileService: CloudFileService) {

    @GetMapping("")
    fun list(model: Model): String {
        val items = cloudFileService.list()
                .map { it.toDTO() }
        model.addAttribute("items", items)
        return "cloud/list"
    }

    @GetMapping("file")
    fun download(@RequestParam name: String): ResponseEntity<Any> {
        val url = cloudFileService.downloadLink(name)
        val headers = HttpHeaders()
        headers.location = url.toURI()
        return ResponseEntity<Any>(headers, FOUND)
    }

    @PostMapping("file/delete")
    @PreAuthorize("hasAuthority('SCOPE_cloudfiles_admin')")
    fun delete(@RequestParam name: String, redirectAttributes: RedirectAttributes): String {
        cloudFileService.deleteFile(name)
        redirectAttributes.addFlashAttribute("message", "Deleted $name")
        return "redirect:/cloud"
    }

}

private fun CloudFileSummary.toDTO() = CloudFileSummaryDTO(name, size.toSizeString())
