package com.canceledsystems.cloudfiles.cloud.web

data class CloudFileSummaryDTO(val name: String, val size: String)