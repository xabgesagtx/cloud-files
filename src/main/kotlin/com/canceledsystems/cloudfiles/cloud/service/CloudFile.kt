package com.canceledsystems.cloudfiles.cloud.service

import java.io.InputStream

class CloudFile(val name: String,
                val mimeType: String,
                val content: InputStream,
                val size: Long
) {
}