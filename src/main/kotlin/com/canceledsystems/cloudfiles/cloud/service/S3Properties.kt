package com.canceledsystems.cloudfiles.cloud.service

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("cloud.s3")
class S3Properties {
    lateinit var key: String
    lateinit var secret: String
    lateinit var bucket: String
    lateinit var region: String
    lateinit var endpoint: String
}