package com.canceledsystems.cloudfiles.cloud.service

import org.springframework.stereotype.Service
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.*
import software.amazon.awssdk.services.s3.presigner.S3Presigner
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest
import java.net.URL
import java.time.Duration

@Service
class CloudFileService(private val s3Client: S3Client,
                       private val s3Presigner: S3Presigner,
                       private val properties: S3Properties) {

    fun save(file: CloudFile) {
        val request = PutObjectRequest.builder()
            .key(file.name)
            .contentType(file.mimeType)
            .contentLength(file.size)
            .contentDisposition("attachment; filename=\"${file.name}\"")
            .bucket(properties.bucket)
            .build()
        val requestBody = RequestBody.fromInputStream(file.content, file.size)
        s3Client.putObject(request, requestBody)
    }

    fun list(): List<CloudFileSummary> {
        val result = mutableListOf<CloudFileSummary>()
        var req = ListObjectsV2Request.builder()
                .bucket(properties.bucket)
                .prefix("")
                .delimiter("/")
                .build()
        var objectListing: ListObjectsV2Response
        do {
            objectListing = s3Client.listObjectsV2(req)
            objectListing.contents()
                    .map { CloudFileSummary(it.key(), it.size()) }
                    .forEach { result.add(it) }
            val token = objectListing.nextContinuationToken()
            req = req.toBuilder()
                .continuationToken(token)
                .build()
        } while (objectListing.isTruncated)
        return result
    }

    fun downloadLink(name: String): URL {
        val getObjectRequest = GetObjectRequest.builder()
            .bucket(properties.bucket)
            .key(name)
            .build()
        val presignRequest = GetObjectPresignRequest.builder()
            .getObjectRequest(getObjectRequest)
            .signatureDuration(Duration.ofSeconds(60 * 15))
            .build()
        return s3Presigner.presignGetObject(presignRequest).url()
    }

    fun deleteFile(name: String) {
        val request = DeleteObjectRequest.builder()
            .bucket(properties.bucket)
            .key(name)
            .build()
        s3Client.deleteObject(request)
    }
}