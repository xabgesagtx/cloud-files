package com.canceledsystems.cloudfiles.cloud.service

data class CloudFileSummary(val name: String,
                            val size: Long)