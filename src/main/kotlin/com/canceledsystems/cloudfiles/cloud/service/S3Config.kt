package com.canceledsystems.cloudfiles.cloud.service

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.presigner.S3Presigner
import java.net.URI

@Configuration
class S3Config {

    @Bean
    fun s3Client(properties: S3Properties): S3Client =
        S3Client.builder()
            .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(properties.key, properties.secret)))
            .endpointOverride(URI.create(properties.endpoint))
            .region(Region.of(properties.region))
            .build()

    @Bean
    fun s3Presigner(properties: S3Properties): S3Presigner =
        S3Presigner.builder()
            .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(properties.key, properties.secret)))
            .endpointOverride(URI.create(properties.endpoint))
            .region(Region.of(properties.region))
            .build()
}