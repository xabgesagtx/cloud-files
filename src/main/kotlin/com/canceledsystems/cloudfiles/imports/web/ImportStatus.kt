package com.canceledsystems.cloudfiles.imports.web

enum class ImportStatus {
    WAITING, RUNNING, SUCCESS, FAILED
}