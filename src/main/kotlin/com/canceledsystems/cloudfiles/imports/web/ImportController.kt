package com.canceledsystems.cloudfiles.imports.web

import com.canceledsystems.cloudfiles.common.toSizeString
import com.canceledsystems.cloudfiles.imports.service.ImportDetails
import com.canceledsystems.cloudfiles.imports.service.ImportService
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("imports")
class ImportController(private val importService: ImportService) {

    @PostMapping(value = ["pme/{fileId}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun import(@PathVariable fileId: String): Map<String, String> {
        importService.importFromPme(fileId)
        return mapOf("message" to "started")
    }

    @GetMapping("")
    fun allImportsList(): String {
        return "imports/list"
    }

    @GetMapping("items")
    fun allImportsItems(model: Model): String {
        val items = importService.allImports().asSequence()
                .sortedByDescending { it.createdAt }
                .map { it.toDTO() }
                .toList()
        model.addAttribute("items", items)
        return "imports/items :: items"
    }
}

private val ImportDetails.status: ImportStatus
    get() {
        return if (startedAt == null && finishedAt == null) {
            ImportStatus.WAITING
        } else if (finishedAt == null) {
            ImportStatus.RUNNING
        } else if (success) {
            ImportStatus.SUCCESS
        } else {
            ImportStatus.FAILED
        }
    }

private val ImportDetails.progress: Double
    get() {
        return if (totalSize == 0L) {
            .0
        } else {
            sizeImported.toDouble() * 100 / totalSize.toDouble()
        }
    }

private fun ImportDetails.toDTO(): ImportDetailsDTO {
    return ImportDetailsDTO(id = id,
            sizeImported = sizeImported.toSizeString(),
            totalSize = totalSize.toSizeString(),
            progress = progress,
            name = name,
            startedAt = startedAt,
            createdAt = createdAt,
            finishedAt = finishedAt,
            status = status,
            mimeType = mimeType)
}
