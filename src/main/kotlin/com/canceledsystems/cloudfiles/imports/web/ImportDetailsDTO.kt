package com.canceledsystems.cloudfiles.imports.web

import java.time.Instant
import java.util.*

data class ImportDetailsDTO(val id: UUID,
                            val name: String,
                            val mimeType: String,
                            val createdAt: Instant,
                            val startedAt: Instant?,
                            val finishedAt: Instant?,
                            val totalSize: String,
                            val sizeImported: String,
                            val progress: Double,
                            val status: ImportStatus)