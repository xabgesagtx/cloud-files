package com.canceledsystems.cloudfiles.imports.service

import java.net.URI
import java.time.Instant
import java.util.*

data class ImportDetails(val id: UUID,
                         val name: String,
                         val url: URI,
                         val createdAt: Instant,
                         val startedAt: Instant?,
                         val finishedAt: Instant?,
                         val totalSize: Long = 0,
                         val sizeImported: Long = 0,
                         val mimeType: String,
                         val success: Boolean)