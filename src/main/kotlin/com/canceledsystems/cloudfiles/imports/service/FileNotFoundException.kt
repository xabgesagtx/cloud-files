package com.canceledsystems.cloudfiles.imports.service

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class FileNotFoundException(message: String) : RuntimeException(message)