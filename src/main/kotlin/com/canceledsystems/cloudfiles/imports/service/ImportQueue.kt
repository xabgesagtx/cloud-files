package com.canceledsystems.cloudfiles.imports.service

import org.springframework.context.event.EventListener
import org.springframework.core.task.TaskExecutor
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.ConcurrentHashMap

@Component
class ImportQueue(private val importExecutor: TaskExecutor) {

    private val jobs = ConcurrentHashMap<UUID, ImportDetails>()

    fun schedule(job: ImportJob) {
        job.sendUpdate()
        importExecutor.execute(job)
    }

    @EventListener
    fun updateDetails(update: ImportJobUpdate) {
         val details = with(update) {
             ImportDetails(id = id,
                     createdAt = createdAt,
                     startedAt = startedAt,
                     finishedAt = finishedAt,
                     sizeImported = sizeImported,
                     totalSize = totalSize,
                     mimeType = mimeType,
                     name = name,
                     url = url,
                     success = success)
        }
        jobs[details.id] = details
    }

    fun allImports() = ArrayList(jobs.values)

}