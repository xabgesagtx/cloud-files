package com.canceledsystems.cloudfiles.imports.service

import com.canceledsystems.cloudfiles.cloud.service.CloudFile
import com.canceledsystems.cloudfiles.cloud.service.CloudFileService
import org.apache.hc.client5.http.classic.methods.HttpGet
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient
import org.apache.hc.core5.http.ClassicHttpResponse
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import software.amazon.awssdk.services.s3.model.S3Exception
import java.io.IOException
import java.net.URI
import java.time.Instant
import java.util.*

class ImportJob(private val id: UUID,
                private val link: URI,
                private val mimeType: String,
                private val cloudFileService: CloudFileService,
                private val httpClient: CloseableHttpClient,
                private val eventPublisher: ApplicationEventPublisher) : Runnable {

    private val log = LoggerFactory.getLogger(javaClass)

    private val createdAt: Instant = Instant.now()

    private val name = link.path.substringAfterLast("/")

    private var finishedAt: Instant? = null
        set(value) {
            field = value
            sendUpdate()
        }

    private var startedAt: Instant? = null
        set(value) {
            field = value
            sendUpdate()
        }
    private var totalSize = 0L
        set(value) {
            field = value
            sendUpdate()
        }


    private var sizeImported = 0L
        set(value) {
            field = value
            sendUpdate()
        }

    private var success = true
        set(value) {
            field = value
            sendUpdate()
        }

    override fun run() {
        log.info("Importing $link of type $mimeType")
        startedAt = Instant.now()
        sendUpdate()
        try {
            httpClient.execute(HttpGet(link)) {
                if (!it.success) {
                    markAsFailed()
                    return@execute
                } else {
                    totalSize = it.size
                    val content = ProgressInputStreamWrapper(it.entity.content) { bytesRead -> sizeImported = bytesRead }
                    val file = CloudFile(name, mimeType, content, totalSize)
                    cloudFileService.save(file)
                    finishedAt = Instant.now()
                    log.info("Finished download of $link")
                }
            }
        } catch (e: IOException) {
            log.error("Failed to import $link", e)
            markAsFailed()
        } catch (e: IllegalStateException) {
            log.error("Failed to import $link", e)
            markAsFailed()
        } catch (e: S3Exception) {
            log.error("Failed to import $link due to S3 issue: ${e.awsErrorDetails()?.errorCode()}", e)
            markAsFailed()
        }
    }

    private fun markAsFailed() {
        success = false
        finishedAt = Instant.now()
    }

    fun sendUpdate() {
        val update = ImportJobUpdate(id = id,
                name = name,
                createdAt = createdAt,
                startedAt = startedAt,
                finishedAt = finishedAt,
                url = link,
                mimeType = mimeType,
                sizeImported = sizeImported,
                totalSize = totalSize,
                success = success)
        eventPublisher.publishEvent(update)
    }

}

private val ClassicHttpResponse.size: Long
    get() = entity.contentLength

private val ClassicHttpResponse.success
    get() = code == 200
