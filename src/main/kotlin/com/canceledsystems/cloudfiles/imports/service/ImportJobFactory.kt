package com.canceledsystems.cloudfiles.imports.service

import org.springframework.beans.factory.config.AutowireCapableBeanFactory
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*

@Component
class ImportJobFactory(private val beanFactory: AutowireCapableBeanFactory) {

    fun create(link: URI, mimeType: String) = beanFactory.getBean(ImportJob::class.java, UUID.randomUUID(), link, mimeType)
}