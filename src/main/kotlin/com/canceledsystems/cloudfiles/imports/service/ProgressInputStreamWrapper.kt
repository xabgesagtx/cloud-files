package com.canceledsystems.cloudfiles.imports.service

import java.io.IOException
import java.io.InputStream


class ProgressInputStreamWrapper(private val inputStream: InputStream,
                                 private val readListener: (Long) -> Unit): InputStream() {

    private var bytesRead = 0L
        set(value) {
            field = value
            readListener.invoke(field)
        }

    @Throws(IOException::class)
    override fun read(): Int {
        val read = inputStream.read()
        if (read != -1) {
            bytesRead++
        }
        return read
    }

    @Throws(IOException::class)
    override fun read(b: ByteArray): Int {
        val read = inputStream.read(b)
        bytesRead += read
        return read
    }

    @Throws(IOException::class)
    override fun read(b: ByteArray, off: Int, len: Int): Int {
        val read = inputStream.read(b, off, len)
        bytesRead += read
        return read
    }

    @Throws(IOException::class)
    override fun skip(n: Long): Long {
        val skipped = inputStream.skip(n)
        bytesRead += skipped
        return skipped
    }

    @Throws(IOException::class)
    override fun close() {
        inputStream.close()
    }
}