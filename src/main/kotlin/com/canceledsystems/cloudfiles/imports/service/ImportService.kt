package com.canceledsystems.cloudfiles.imports.service

import com.canceledsystems.cloudfiles.pme.service.PmeFileService
import org.springframework.stereotype.Service

@Service
class ImportService(private val pmeFileService: PmeFileService,
                    private val jobFactory: ImportJobFactory,
                    private val importQueue: ImportQueue) {


    fun importFromPme(fileId: String) {
        val file = pmeFileService.fileDetails(fileId)
                ?: throw FileNotFoundException("Could not find file $fileId to import")
        val job = jobFactory.create(file.link, file.mimeType)
        importQueue.schedule(job)
    }

    fun allImports(): List<ImportDetails> {
        return importQueue.allImports()
    }
}