package com.canceledsystems.cloudfiles.imports.service

import java.net.URI
import java.time.Instant
import java.util.*

data class ImportJobUpdate(val id: UUID,
                           val name: String,
                           val url: URI,
                           val createdAt: Instant,
                           val finishedAt: Instant?,
                           val totalSize: Long,
                           val sizeImported: Long,
                           val mimeType: String,
                           val startedAt: Instant?,
                           val success: Boolean)