package com.canceledsystems.cloudfiles.imports.service

import com.canceledsystems.cloudfiles.cloud.service.CloudFileService
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient
import org.apache.hc.client5.http.impl.classic.HttpClients
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import java.net.URI
import java.util.*

@Configuration
class ImportConfiguration(private val cloudFileService: CloudFileService,
                          private val eventPublisher: ApplicationEventPublisher) {

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    fun importJob(id: UUID, link: URI, mimeType: String) = ImportJob(id, link, mimeType, cloudFileService, httpClient(), eventPublisher)

    @Bean
    fun httpClient(): CloseableHttpClient = HttpClients.createDefault()

    @Bean
    fun importExecutor() = ThreadPoolTaskExecutor()

}