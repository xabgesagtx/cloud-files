# Cloud files

[![pipeline status](https://gitlab.com/xabgesagtx/cloud-files/badges/master/pipeline.svg)](https://gitlab.com/xabgesagtx/cloud-files/commits/master)

Provides an interface to import files from your premiumize.me cloud to your own S3 to share with others - secured by OAuth2.

Features:
* browse your premiumize cloud
* import files from premiumize to S3 asynchronously (admin users)
* monitor imports and their progress (admin users)
* download files from S3 using presigned URLs
* configurable OAuth2 based authentication

Technologies used:
* spring boot based application
* written in kotlin
* using spring security for authentication
* thymeleaf and bootstrap for UI
* and amazon SDK for S3 access

## Dependencies

* java 21 for building
* docker or java 21 for running
* S3 bucket for file storage
* premiumize.me account and API key
* OAuth2 identity provider for authentication

## Build

Cloud files comes with a gradle wrapper to build the application as jar

```
./gradlew build
```

## Configuration

As cloud files depends on external systems to provide authentication and access to S3 and premiumize.me. There is some configuration needed.

The easiest way for a spring boot application is to create an application.yml

```yaml
cloud:
  s3:
    key: KEY_ID
    secret: KEY_SECRET
    bucket: BUCKET
    region: REGION
    endpoint: S3_ENDPOINT
pme:
  api:
    key: PREMIUMIZE_API_KEY
    baseUrl: https://www.premiumize.me/api/
spring:
  security:
    oauth2:
      client:
        registration:
          keycloak:
            client-id: OAUTH_CLIENT_ID
            client-secret: OAUTH_CLIENT_SECRET
            authorization-grant-type: authorization_code
            redirect-uri: '{baseUrl}/login/oauth2/code/{registrationId}'
            scope:
              - openid
              - profile
              - email
              - cloudfiles
        provider:
          keycloak:
            issuer-uri: YOUR_OAUTH_ISSUER_URI
            user-name-attribute: preferred_username

security:
  logoutSuccessUrl: URL_TO_REDIRECT_CLIENT_AFTER_LOGOUT
```

### S3

All properties here are specific to the project and defined in the class `S3Properties`. The properties should make you able to run this project with AWS S3, Digital Ocean Spaces or any other S3 compliant API.

### OAuth2

The configuration options outlined here are just an example configuration. More details can be found in the [spring security documentation](https://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#oauth2login-sample-application-config).

It's important to note that there has to be the scope `cloudfiles` provided for the user that is allowed to access the application.

Additionally, there is an admin scope needed for users to browse premiumize.me and import files.

## Development

To run in development you have to provide all the configuration options outlined under configuration.

## Run in production

### Jar

You can either build the application yourself and run the jar:

```
java -jar build/libs/cloud-files.jar
```

Make sure you put the application.yml with the required configurations in the folder where you run this command.

### Docker

Alternatively, you can use the docker image from gitlab:

```
docker run -p "8080:8080" -v PATH_TO_APPLICATION_YML:/application.yml registry.gitlab.com/xabgesagtx/cloud-files
```
