ARG BUILD_HOME=/build-home

## build container
FROM maven:3.9.7-eclipse-temurin-21 as build-image

ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
WORKDIR $APP_HOME

COPY pom.xml $APP_HOME/
COPY src $APP_HOME/src

RUN mvn package -DskipTests=true


## run container
FROM eclipse-temurin:21-alpine

ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
ENV JAVA_OPTS=""
COPY --from=build-image $APP_HOME/target/cloud-files.jar app.jar

ENTRYPOINT java $JAVA_OPTS -jar app.jar